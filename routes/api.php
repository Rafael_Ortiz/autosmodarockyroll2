<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MarcasController;
use App\Http\Controllers\AutosController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('auth/register',[AuthController::class, 'create']);
Route::post('auth/login',[AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function(){
    Route::resource('marcas',MarcasController::class);
    Route::resource('autos',AutosController::class);
    Route::get('autoall',[AutosController::class, 'all']);
    Route::get('autobymarca',[AutosController::class, 'AutoByMarca']);
    Route::get('auth/logout',[AuthController::class, 'logout']);
});

